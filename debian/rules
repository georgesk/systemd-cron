#!/usr/bin/make -f

# gbp import-orig --pristine-tar --uscan

DEB_BUILD_HARDENING=1

%:
	dh $@ --with python3

override_dh_auto_configure:
	./configure \
--prefix=/usr \
--unitdir=/lib/systemd/system \
--confdir=/etc \
--statedir=/var/spool/cron/crontabs \
--runparts="/bin/run-parts --report" \
--bindir=/usr/bin \
--libdir=/lib \
--enable-boot=no \
--enable-setgid=yes \
--enable-persistent=yes

override_dh_installsystemd:
	# Only enable+start cron.target, it pulls in all the others via .timer files.
	dh_installsystemd cron.target

override_dh_clean:
	rm -f Makefile
	dh_clean

override_dh_auto_test:
	# tests disabled for now to avoid this bug on pbuilder:
	# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=555331#40
	# they duplicate lintian check "manpage-has-errors-from-man" anyway

override_dh_install:
	dh_install
	sed -i '/Persistent=true/d' debian/systemd-cron/lib/systemd/system/cron-hourly.timer
	sed -i 's/OnCalendar=hourly/OnCalendar=*-*-* *:17:00/' debian/systemd-cron/lib/systemd/system/cron-hourly.timer
	sed -i 's/OnCalendar=daily/OnCalendar=*-*-* 06:25:00/' debian/systemd-cron/lib/systemd/system/cron-daily.timer
	sed -i 's/OnCalendar=weekly/OnCalendar=Mon *-*-* 06:47:00/' debian/systemd-cron/lib/systemd/system/cron-weekly.timer
	sed -i 's/OnCalendar=monthly/OnCalendar=*-*-1 06:52:00/' debian/systemd-cron/lib/systemd/system/cron-monthly.timer
	echo 'ExecStartPre=/lib/systemd-cron/boot_delay 5' >> debian/systemd-cron/lib/systemd/system/cron-daily.service
	echo 'ExecStartPre=/lib/systemd-cron/boot_delay 10' >> debian/systemd-cron/lib/systemd/system/cron-weekly.service
	echo 'ExecStartPre=/lib/systemd-cron/boot_delay 15' >> debian/systemd-cron/lib/systemd/system/cron-monthly.service

	# we rely on debhelper magic
	rm -rvf debian/systemd-cron/lib/sysusers.d/
	dh_installsysusers
